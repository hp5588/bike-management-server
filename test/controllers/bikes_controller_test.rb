require 'test_helper'

class BikesControllerTest < ActionDispatch::IntegrationTest
  test "should get available" do
    get bikes_available_url
    assert_response :success
  end

end
