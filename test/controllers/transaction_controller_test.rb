require 'test_helper'

class TransactionControllerTest < ActionDispatch::IntegrationTest
  test "should get verify" do
    get transaction_verify_url
    assert_response :success
  end

end
