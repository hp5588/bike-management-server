class ReservationsController < ApplicationController


  def monthly
    # @result = []
    # @reservations  = Reservation.where(:reserved_date => Date.today..30.days.from_now).each do |reservation|
    #   @result << {reservations: reservation, customers: reservation.customer, bikes: reservation.bike}
    # end
    # render json:@result
    #
    @reservations  = Reservation.where(:reservation_rent_date => Date.today..30.days.from_now).joins(:customer).joins(:bike).select("*")

    render json:@reservations
  end



  private
  def tomorrow_params
    params.permit(:reserved_date)
  end
end
