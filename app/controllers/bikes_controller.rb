class BikesController < ApplicationController


  def profile
    @bike = Bike.where(bike_id_number: profile_params[:bike_id_number]).first
    @transaction  = @bike.transactions.last
    @customer = @transaction.customer

    @result =  [ @bike , @customer , @transaction]
    render json: @result
  end

  def overview
    # @bikes = Bike.joins_transaction_latest
    @bikes = Bike.joins(:transactions).select('DISTINCT ON (bikes.id) bikes.*', 'transactions.*').order('bikes.id, transactions.created_at DESC')

    @repairBikes = @bikes.select{|t| t.status == 0}
    @insideBikes = @bikes.select{|t| t.status == 1}
    @outsideBikes = @bikes.select{|t| t.status == 2}

    @repairCount = @repairBikes.count
    @insideCount = @insideBikes.count
    @outsideCount = @outsideBikes.count


    @availableCount = {}

    7.times do |count|
      @count = count
      @selectedCount = @insideBikes.select{|t| t.category == count}.count
      @categoryCount = @bikes.select{|t| t.category == count}.count

      @availableCount[count] = {@selectedCount => @categoryCount}
    end

    @result = []

    @result << {insideCount: @insideCount , outsideCount: @outsideCount, repairCount: @repairCount, availableCount: @availableCount}


    render json: @result
  end

  def records
    render json: Bike.where(bike_id_number: records_params[:bike_id_number]).first.transactions.order(:created_at)
  end

  def available
    @specificKindOfBikes = Bike.where(category: available_params[:category])

    @availableTransaction = Transaction.where(bike_id: @specificKindOfBikes.ids).group(:bike_id).order(:created_at)
    @selectedTransaction =  @availableTransaction.select{ |transaction| transaction.status == 1}
    @results
    @availableBikes = @selectedTransaction.each do |transaction|
      @bike = Bike.where(id: transaction[:bike_id])
      if @results.blank?
        @results = @bike
      else
        @results = @results | @bike
      end
    end
    render json: @results
  end

  def list
    render json: Bike.all
  end


  def rent
    @bike = Bike.where(bike_id_number: rent_params[:bike_id_number]).first
    @bike.transactions.create(customer_id: rent_params[:customer_id],
                              rent_date: rent_params[:rent_date],
                              return_date: rent_params[:return_date],
                              status: 2)
    head :ok, content_type: "text/html"
  end

  def Return
    @bike = Bike.where(bike_id_number: rent_params[:bike_id_number]).first
    @bike.transactions.create(customer_id: 1,status: 1)
    head :ok, content_type: "text/html"
  end

  def repair
    @bike = Bike.where(bike_id_number: repair_params[:bike_id_number]).first
    @bike.transactions.create(customer_id: 1,rent_date: DateTime.now ,status: 0)
    head :ok, content_type: "text/html"
  end

  def add

  end

  def remove

  end

  def return_list_today
    render json: Transaction.left_joins(:bike).left_joins(:customer).select('DISTINCT ON (bikes.id) *', :id, :status, :created_at, :updated_at)
                     .where(rent_date: Date.today.all_day).order('bikes.id, transactions.created_at DESC')
                     .select{|t| t.status == 2}
  end

  def prepare_list_today
    @result = []
    @reservations  = Reservation.where(:reservation_rent_date => Date.today.beginning_of_day..Date.today.end_of_day).each do |reservation|
      @result << {reservations: reservation, customers: reservation.customer, bikes: reservation.bike}
    end

    render json:@result
  end



  def total

  end



  private
  def available_params
    params.permit(:category)
  end

  def bike_id_number_params
    params.permit(:bike_id_number)
  end


  def profile_params
    params.permit(:bike_id_number)
  end

  def records_params
    params.permit(:bike_id_number)
  end

  def rent_params
    params.permit(:bike_id_number, :customer_id, :rent_date, :return_date)
  end

  def repair_params
    params.permit(:bike_id_number)
  end

  def test_join_params
    params.permit(:bike_id_number)
  end

end
