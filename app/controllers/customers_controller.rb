class CustomersController < ApplicationController


  def new
    @customer = Customer.new(customer_params)
    @customer.save
  end

  def create

  end

  # def show
  #   @article = Customer.find(params[:id_number])
  # end

  private
  def customer_params
    params.permit(:last_name,:first_name,:id_number)
  end
end
