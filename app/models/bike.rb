class Bike < ApplicationRecord
  has_many :transactions
  has_many :reservations
  has_many :customers, through: :transactions


  def self.joins_transaction_latest
    query = <<-SQL
      WITH transactions AS (SELECT DISTINCT ON (transactions.bike_id) * FROM transactions ORDER BY transactions.bike_id, "transactions"."created_at" DESC)
      SELECT transactions.*, bikes.category, bikes.bike_id_number FROM transactions INNER JOIN bikes ON "transactions"."bike_id" = "bikes"."id"
    SQL

    self.find_by_sql(query)

  end
end