class Customer < ApplicationRecord
  has_many :transactions
  has_many :reservations
  has_many :bikes, through: :transactions
end
