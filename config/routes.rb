Rails.application.routes.draw do
  get 'bikes/available'
  get 'transactions/verify'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  #queries
  get 'bikes/available'
  get 'bikes/profile'
  get 'bikes/records'
  get 'bikes/return_list_today'
  get 'bikes/prepare_list_today'
  get 'bikes/list'

  #actions
  get 'bikes/overview'
  get 'bikes/rent'
  get 'bikes/Return'
  get 'bikes/repair'
  get 'bikes/remove'
  get 'bikes/add'

  get 'reservations/monthly'

  resources :transactions
  resources :customers

end
