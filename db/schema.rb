# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_07_08_204044) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "bikes", force: :cascade do |t|
    t.string "bike_id_number"
    t.integer "category"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["bike_id_number"], name: "index_bikes_on_bike_id_number", unique: true
  end

  create_table "customers", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.string "id_number"
    t.string "bank_account"
    t.string "phone_number"
    t.integer "stars"
    t.integer "customer_status"
    t.string "address"
    t.string "nationality"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["id_number"], name: "index_customers_on_id_number", unique: true
  end

  create_table "reservations", force: :cascade do |t|
    t.datetime "reservation_rent_date"
    t.datetime "reservation_return_date"
    t.bigint "customer_id"
    t.bigint "bike_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["bike_id"], name: "index_reservations_on_bike_id"
    t.index ["customer_id"], name: "index_reservations_on_customer_id"
  end

  create_table "transactions", force: :cascade do |t|
    t.bigint "customer_id"
    t.bigint "bike_id"
    t.datetime "rent_date"
    t.datetime "return_date"
    t.integer "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["bike_id"], name: "index_transactions_on_bike_id"
    t.index ["customer_id"], name: "index_transactions_on_customer_id"
  end

end
