class CreateCustomers < ActiveRecord::Migration[5.2]
  def change
    create_table :customers do |t|

      t.string :first_name
      t.string :last_name
      t.string :id_number, index: { unique: true }
      t.string :bank_account
      t.string :phone_number

      t.integer :stars
      t.integer :customer_status
      t.string :address
      t.string :nationality

      t.timestamps
    end
    # add_index :customers, :id_number, unique: true
  end
end
