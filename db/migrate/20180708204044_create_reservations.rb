class CreateReservations < ActiveRecord::Migration[5.2]
  def change
    create_table :reservations do |t|
      t.datetime :reservation_rent_date
      t.datetime :reservation_return_date
      t.belongs_to :customer, index: true
      t.belongs_to :bike, index: true
      t.timestamps
    end
  end
end
