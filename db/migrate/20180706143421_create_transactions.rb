class CreateTransactions < ActiveRecord::Migration[5.2]
  def change
    create_table :transactions do |t|
      t.belongs_to :customer,  index: true
      t.belongs_to :bike, index: true
      t.datetime :rent_date
      t.datetime :return_date
      t.integer :status
      t.timestamps
    end
  end
end
