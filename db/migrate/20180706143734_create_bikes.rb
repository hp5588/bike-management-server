class CreateBikes < ActiveRecord::Migration[5.2]
  def change
    create_table :bikes do |t|
      t.string :bike_id_number, index: { unique: true }
      t.integer :category

      t.timestamps
    end

    # add_index :bikes, :bike_id_number, unique: true

  end
end
