# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
#


require 'faker'
include Faker
50.times do
  Bike.create(
      bike_id_number: Faker::IDNumber.valid,
      category: Faker::Number.between(0,6)
  )
end

Customer.create(
    first_name: "Freiburg",
    last_name: "Bike",
    id_number: "000000000",
    # bank_account: Faker::Bank.iban,
    # phone_number: Faker::PhoneNumber.cell_phone,
    # stars: Faker::Number.between(0,4),
    # status: Faker::Number.between(0,2),
    # address: Faker::Address.full_address,
    # nationality: Faker::Address.country_code_long,
    )

50.times do
  Customer.create(
      first_name: Faker::Name.first_name,
      last_name: Faker::Name.last_name,
      id_number: Faker::IDNumber.valid,
      bank_account: Faker::Bank.iban,
      phone_number: Faker::PhoneNumber.cell_phone,
      stars: Faker::Number.between(0,4),
      customer_status: Faker::Number.between(0,2),
      address: Faker::Address.full_address,
      nationality: Faker::Address.country_code_long,
      )
end



200.times do
  @date = Faker::Date.backward(5) + 1.days
    Transaction.create(
        bike_id: Faker::Number.between(1,Bike.count),
        customer_id: Faker::Number.between(1,Customer.count),
        rent_date: @date + 1.days,
        return_date: Faker::Date.between(@date - 2.days, @date - 2.days + Faker::Number.between(0, 10).days ),
        status: Faker::Number.between(0,2) #REPAIR,IN, OUT
    )
end



40.times do
  @date = Faker::Date.between(1.days.from_now, 30.days.from_now)
  Reservation.create(
      bike_id: Faker::Number.between(1,Bike.count),
      customer_id: Faker::Number.between(1,Customer.count),
      # reserved_date: Faker::Date.between(1.days.from_now, 30.days.from_now),
      reservation_rent_date: @date,
      reservation_return_date: Faker::Date.between(@date, @date + Faker::Number.between(0, 7).days)
  )
end

